import math

def air_ordonne(a, b, c):
    #on definit u3 valeur maximale
  u3 = max(a,b,c)
    #si a c'est le max
  if (u3 == a):
      #on difinit u1 valeur min
    u1 =min(b,c)
      #si b c'est le min
    if(u1==b):
        #donc u2 soit c'est c
        u2 = c
    else:
        #  soit c'est b
        u2 = b
        # si b c'est le max
  elif(u3 == b):
    u1 = min(a, c)
    # si b c'est le min
    if (u1 == a):
        # donc u2 soit c'est c
        u2 = c
    else:
        #  soit c'est a
        u2 = a
        # si c c'est le max
  elif(u3 == c):
    u1 = min(a, b)
    # si a c'est le min
    if (u1 == a):
        # donc u2 soit c'est b
        u2 = b
  else:
      #  soit c'est a
        u2 = a
        #calcul de la fonction
  d = math.pow(u1,2) * math.pow(u3,2)
  e = (math.pow(u1,2) - math.pow(u2,2) + math.pow(u3,2)) / 2
  f = d - math.pow(e,2)
  resultat = (math.sqrt(f)) / 2
  return (resultat)


print (air_ordonne(4,3,2))

print(air_ordonne(4,3,3))

print(air_ordonne(4,4,4))

print(air_ordonne(3,4,5))

print(air_ordonne(13,14,15))

print(air_ordonne(1,1,1))