import math


# calcul du perimetre
def perimetre(a, b, c):
    return (a + b + c)


# def de triangle
def definit_triangle(a, b, c):

    if (a < 0 or b < 0 or c < 0):
        return (False)
    x = (a + b + c) / 2
    if (a >= x or b >= x or c >= x):
        return (False)
    return (True)


# def air du triangle
def air_ordonne(a, b, c):

    u3 = max(a, b, c)
    if (u3 == a):
        u1 = min(b, c)
        if (u1 == b):
            u2 = c
        else:
            u2 = b
    elif (u3 == b):
        u1 = min(a, c)
        if (u1 == a):
            u2 = c
        else:
            u2 = c
    elif (u3 == c):
        u1 = min(a, b)
        if (u1 == a):
            u2 = b
        else:
            u2 = a

    x = math.pow(u1, 2) * math.pow(u3, 2)
    y = (math.pow(u1, 2) - math.pow(u2, 2) + math.pow(u3, 2)) / 2
    z = x - math.pow(y, 2)

    if (z < 0):
        return (-1)
    resultat = (math.sqrt(z)) / 2
    return (resultat)



def check_duplicat(i, j, k):
    if (i <= j and j <= k):
        return (True)
    return (False)



def nb_triangles_speciaux(n, p):
    if (n < 0 or p < 0):
        return ("n et p doit etre positive")
    if (n > p):
        return ("n ne doit pas etre superieur a p")
    i = n
    ret = 0
    while (i <= p):
        j = n
        while (j <= p):
            k = n
            while (k <= p):
                if (definit_triangle(i, j, k)):
                    if (air_ordonne(i, j, k) == perimetre(i, j, k)):
                        if (check_duplicat(i, j, k)):
                            print("(", i, ",", j, ",", k, ")")
                            ret += 1
                k += 1
            j += 1
        i += 1

    return (ret)


print("result : ", nb_triangles_speciaux(1, 20))
